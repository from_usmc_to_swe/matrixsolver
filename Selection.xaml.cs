﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixSolver
{
    /// <summary>
    /// Interaction logic for Selection.xaml
    /// </summary>
    public partial class Selection : Window
    {
        public Selection()
        {
            InitializeComponent();
        }

        private void Tripblebtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new();
            mainWindow.Show();
            Hide();
            Close();
        }

        private void doublebtn_Click(object sender, RoutedEventArgs e)
        {
            MatrixDouble doubleMatrix = new();
            doubleMatrix.Show();
            Hide();
            Close();
        }

        private void quadbtn_Click(object sender, RoutedEventArgs e)
        {
            QuadMatrix quadMatrix = new();
            quadMatrix.Show();
            Hide();
            Close();
        }
    }
}
