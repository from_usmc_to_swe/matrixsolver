﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MatrixSolver
{
    class HelperFunctions
    {

        private int numRows;
        private int numColumns;
        private Grid myGrid = null;
        private Window window = null;
        private const int HEADER_ROW = 1;
        private const int HEADER_COL = 1;
        private const int START_ROW = 2;
        private const int START_COL = 2;
        public string StoredPivotName { get; set; }
        public bool SelectPivot { get; set; }
        public bool ReadyToPivot{ get; set;}
        public int PivotRow { get; set; }
        public int PivotCol { get; set; }
        public double PivotValue { get; set; }

        public const int ROUND_PRECISION = 4;

        public HelperFunctions(int numRows, int numColumns, Grid grid, Window window)
        {
            this.numRows = numRows;
            this.numColumns = numColumns;
            myGrid = grid;
            this.window = window;

        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {

            int index;
            for (index = 0; index < VisualTreeHelper.GetChildrenCount(depObj); index++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(depObj, index);

                if (child is not null and T)
                {
                    yield return (T)child;
                }

                foreach (T childOfChild in FindVisualChildren<T>(child))
                {
                    yield return childOfChild;
                }
            }
        }

        public void ClearMatrix()
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(window))
            {
                if (!tb.Name.Contains("Label") && !Grid.GetRow(tb).Equals(numRows + 1) && !Grid.GetColumn(tb).Equals(numColumns + 1))
                {
                    tb.Text = "";
                    tb.IsReadOnly = false;
                    tb.Background = Brushes.Transparent;
                }
                else if (tb.Name.Contains("Label") && Grid.GetRow(tb).Equals(1))
                {
                    tb.Text = $"Y{Grid.GetColumn(tb) - 1}";

                }
                else if (tb.Name.Contains("Label") && Grid.GetColumn(tb).Equals(1))
                {
                    tb.Text = $"X{Grid.GetRow(tb) - 1}";

                }
                else if (!tb.Name.Contains("Label") && !Grid.GetRow(tb).Equals(numRows + 1) && Grid.GetColumn(tb).Equals(numColumns + 1))
                {

                    tb.Text = "1";
                }
                else if (!tb.Name.Contains("Label") && Grid.GetRow(tb).Equals(numRows + 1) && !Grid.GetColumn(tb).Equals(numColumns + 1))
                {

                    tb.Text = "-1";
                }
                else
                {
                    tb.Text = "0";
                }


            }

            SelectPivot = false;
            ReadyToPivot = false;

        }

        public TextBox GetPivotTextBox()
        {

            foreach (TextBox tb in FindVisualChildren<TextBox>(window))
            {
                if (tb.Name.Equals("PivotBox", StringComparison.Ordinal))
                {
                    return tb;
                }
            }

            return null;

        }

        public TextBox GetTextBox(int row, int col)
        {
            return myGrid.Children
           .Cast<TextBox>()
           .First(e => Grid.GetRow(e) == row && Grid.GetColumn(e) == col);
        }

        public double GetCellValue(int row, int column)
        {

            TextBox box = myGrid.Children
             .Cast<TextBox>()
             .First(e => Grid.GetRow(e) == row && Grid.GetColumn(e) == column);

            return double.Parse(box.Text);
        }

        public void FlipLabels(TextBox pivotBox)
        {

            int pivotRow = Grid.GetRow(pivotBox);
            int pivotCol = Grid.GetColumn(pivotBox);


            TextBox ColumnHeader = GetTextBox(HEADER_ROW, pivotCol);

            TextBox RowHeader = GetTextBox(pivotRow, HEADER_COL);
            string temp = "";

            temp = ColumnHeader.Text;
            ColumnHeader.Text = RowHeader.Text;
            RowHeader.Text = temp;

            temp = null;


        }

        public bool CheckForEmptyCells()
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(window))
            {

                if (tb.Text.Equals("", StringComparison.Ordinal))
                {
                    return false;
                }
            }

            return true;
        }


        private List<TextBox> GetPivotColumns()
        {
            List<TextBox> PivotColumns = new();

            foreach (TextBox tb in FindVisualChildren<TextBox>(window))
            {

                if (!tb.Name.Equals("PivotBox", StringComparison.Ordinal) && Grid.GetColumn(tb).Equals(PivotCol) && !tb.Name.Contains("Label"))
                {
                    PivotColumns.Add(tb);
                }

            }

            return PivotColumns;
        }

        private List<TextBox> GetPivotRows()
        {
            List<TextBox> PivotRows = new();

            foreach (var tb in FindVisualChildren<TextBox>(window))
            {

                if (!tb.Name.Equals("PivotBox", StringComparison.Ordinal) && Grid.GetRow(tb).Equals(PivotRow) && !tb.Name.Contains("Label"))

                {
                    PivotRows.Add(tb);
                }
            }

            return PivotRows;

        }

        private List<TextBox> GetNonePivotCells()
        {
            List<TextBox> NotPivotRows = new();

            foreach (TextBox tb in FindVisualChildren<TextBox>(window))
            {

                if (!tb.Name.Equals("PivotBox", StringComparison.Ordinal) && !Grid.GetRow(tb).Equals(PivotRow) && !Grid.GetColumn(tb).Equals(PivotCol) && !tb.Name.Contains("Label"))
                {
                    NotPivotRows.Add(tb);
                }
            }

            return NotPivotRows;
        }


        public void SuggestPivotColumns(int row, int column)
        {

            for (int i = 0; i < numColumns - 1; i++)
            {
                if (double.Parse(GetTextBox(numRows + 1, column + i).Text) < 0)
                {
                    HighLightPivotRow(row, column + i);
                }
            }

        }

        private void HighLightPivotRow(int row, int column)
        {
            int index;
            double[] rowArray = new double[numRows];
            for (index = 0; index < numRows - 1; index++)
            {
                rowArray[index] = GetRowRatio(row + index, column);
            }

            double rowMin = MinArray(rowArray);

            int rowIndex;
            for (rowIndex = 0; rowIndex < numRows - 1; rowIndex++)
            {
                if (rowArray[rowIndex] == rowMin)
                {
                    GetTextBox(row + rowIndex, column).Background = Brushes.Yellow;
                }
            }

        }


        private static double MinArray(double[] array)
        {
            double minNum = double.MaxValue;

            foreach (double i in array)
            {
                if (i < minNum && i > 0)
                {
                    minNum = i;
                }
            }

            return minNum;
        }

        private double GetRowRatio(int row, int column)
        {
            var box = GetTextBox(row, numColumns + 1);
            var compareBox = GetTextBox(row, column);

            var ratio = Math.Round(double.Parse(box.Text) / double.Parse(compareBox.Text), ROUND_PRECISION + 1);

            if (ratio > 0)
            {
                return ratio;
            }
            else
            {
                return 100;
            }
        }

        public void HandleSelectPivotClick()
        {
            if (CheckForEmptyCells() && !SelectPivot)
            {
                SuggestPivotColumns(START_ROW, START_COL);
                window.Cursor = Cursors.Hand;
                SelectPivot = true;

                foreach (var tb in FindVisualChildren<TextBox>(window))
                {
                    tb.IsReadOnly = true;
                }

            }
        }

        public void HandlePivotSelect(TextBox box)
        {
            if (SelectPivot)
            {
                PivotValue = double.Parse(box.Text);
                PivotCol = Grid.GetColumn(box);
                PivotRow = Grid.GetRow(box);

                box.Background = Brushes.Red;
                SelectPivot = false;
                ReadyToPivot = true;
                window.Cursor = Cursors.Arrow;

                StoredPivotName = box.Name;
                box.Name = "PivotBox";
            }
        }

        public void HandlePivot()
        {
            if (!SelectPivot && ReadyToPivot)
            {
                TextBox pivotBox = GetPivotTextBox();

                // Swaps the Labels for the row and column
                FlipLabels(pivotBox);

                foreach (TextBox NotPivotCell in GetNonePivotCells())
                {
                    NotPivotCell.Background = Brushes.Transparent;
                    double RowValue = GetCellValue(PivotRow, Grid.GetColumn(NotPivotCell));
                    double ColumnValue = GetCellValue(Grid.GetRow(NotPivotCell), PivotCol);

                    double cellValue = double.Parse(NotPivotCell.Text);

                    NotPivotCell.Text = Math.Round(cellValue - (RowValue * ColumnValue) / PivotValue, ROUND_PRECISION).ToString();

                }

                pivotBox.Text = Math.Round(1 / PivotValue, ROUND_PRECISION).ToString();

                foreach (TextBox textbox in GetPivotColumns())
                {
                    textbox.Background = Brushes.Transparent;
                    textbox.Text = Math.Round(-double.Parse(textbox.Text) / PivotValue, ROUND_PRECISION).ToString();
                }

                foreach (TextBox rowbox in GetPivotRows())
                {
                    rowbox.Background = Brushes.Transparent;
                    rowbox.Text = Math.Round(double.Parse(rowbox.Text) / PivotValue, ROUND_PRECISION).ToString();
                }

                pivotBox.Background = Brushes.Transparent;
                pivotBox.Name = StoredPivotName;
                ReadyToPivot = false;
            }
        }


    }
}
