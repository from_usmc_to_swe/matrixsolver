﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixSolver
{
    /// <summary>
    /// Interaction logic for QuadMatrix.xaml
    /// </summary>
    public partial class QuadMatrix : Window
    {
        static readonly int numColumns = 5;
        static readonly int numRows = 5;


        HelperFunctions helperFunctions;

        public QuadMatrix()
        {
            InitializeComponent();
            helperFunctions = new(numRows, numColumns, myGrid, this);
        }

        private void Select_Pivot_Btn_Click(object sender, RoutedEventArgs e)
        {
            helperFunctions.HandleSelectPivotClick();
        }

        private void Matrix_0_0_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_0_0);
        }

        private void Matrix_0_1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_0_1);
        }

        private void Matrix_0_2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_0_2);
        }

        private void Matrix_0_3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_0_3);
        }

        private void Matrix_1_0_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_1_0);
        }

        private void Matrix_1_1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_1_1);
        }

    
        private void Matrix_1_2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_1_2);
        }

        private void Matrix_1_3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_1_3);
        }

        private void Matrix_2_0_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_2_0);
        }

        private void Matrix_2_1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_2_1);
        }

        private void Matrix_2_2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_2_2);
        }

        private void Matrix_2_3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_2_3);
        }

        private void Matrix_3_0_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_3_0);
        }

        private void Matrix_3_1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_3_1);
        }

        private void Matrix_3_2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_3_2);
        }

        private void Matrix_3_3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            helperFunctions.HandlePivotSelect(Matrix_3_3);
        }


        private void Pivot_Click(object sender, RoutedEventArgs e)
        {
            helperFunctions.HandlePivot();

        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            helperFunctions.ClearMatrix();

        }

        private void MainBtn_Click(object sender, RoutedEventArgs e)
        {
            helperFunctions.ClearMatrix();
            Selection selectionWindow = new();
            selectionWindow.Show();
            Hide();
            Close();

        }

    }

}
